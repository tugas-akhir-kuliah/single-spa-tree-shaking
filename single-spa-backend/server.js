const dotEnvOption = {
  silent: true,
  path: "env/dev.env",
};

if (process.env.NODE_ENV === "production") {
  dotEnvOption.path = "env/prod.env";
}

require("dotenv").config(dotEnvOption);

// Load dependency
const http = require("http");
const express = require("express");
const cors = require("cors");

// Create express instance
const app = express();

// Define app port
const appPort = process.env.PORT || 8080;
app.set("port", appPort);

// Set security middleware
app.use(cors({ origin: "*" }));

// Dummy data for articles
const article = [
  { id: "123452", name: "Toothbrush", price: "€ 126.00", active: true },
  { id: "821934", name: "Chair", price: "€ 124.87", active: false },
];

// Dummy data for sales
const sales = [
  { id: "1", articleId: "123452", quantity: 5, completed: true },
  { id: "2", articleId: "821934", quantity: 56, completed: false },
];

// Set route
app.get("/article", (req, res) => {
  res.json(article);
});

app.get("/sales", (req, res) => {
  res.json(sales);
});

// Create web server
http
  .createServer(app)
  .listen(appPort, () =>
    console.log(`Node app running at localhost::${appPort}`)
  );
